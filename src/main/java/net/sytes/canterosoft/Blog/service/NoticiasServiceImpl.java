package net.sytes.canterosoft.Blog.service;

import net.sytes.canterosoft.Blog.dao.INoticiasDao;
import net.sytes.canterosoft.Blog.entity.Noticias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticiasServiceImpl implements INoticiasService{

    @Autowired
    INoticiasDao iNoticiasDao;

    @Override
    public List<Noticias> listarNoticias() {
        return iNoticiasDao.cargaNoticias();
    }
}
