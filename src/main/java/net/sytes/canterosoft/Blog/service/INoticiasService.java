package net.sytes.canterosoft.Blog.service;

import net.sytes.canterosoft.Blog.entity.Noticias;

import java.util.List;

public interface INoticiasService {
    public List<Noticias> listarNoticias();
}
