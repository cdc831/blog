package net.sytes.canterosoft.Blog.controller;

import net.sytes.canterosoft.Blog.entity.Noticias;
import net.sytes.canterosoft.Blog.service.NoticiasServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private NoticiasServiceImpl noticiasServiceImpl;

    @GetMapping({"/index","/","home"})
    public String index(Model model){
        List<Noticias> listaNoticias = noticiasServiceImpl.listarNoticias();
        model.addAttribute("listaDeNoticias",listaNoticias);
        return "index";
    }

    @GetMapping("/sobremi")
    public String sobreMi(){
        return "about";
    }

    @GetMapping("/admin")
    public String admin(){
        return "admin/index";
    }
}
