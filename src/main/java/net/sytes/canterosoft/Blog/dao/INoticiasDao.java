package net.sytes.canterosoft.Blog.dao;

import net.sytes.canterosoft.Blog.entity.Noticias;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface INoticiasDao extends JpaRepository<Noticias,Long> {

    @Query(
            value = "SELECT * FROM tblNoticias ORDER BY ID DESC",
            nativeQuery = true
    )
    List<Noticias> cargaNoticias();


    /*

    @Query(value = "SELECT p FROM Persona p WHERE p.nombre LIKE %:filtro% OR p.apellido LIKE %:filtro%")
    List<Persona> search(@Param("filtro") String filtro);

    Ejemplo con parametros.
    @Query(
            value = "SELECT * FROM persona WHERE persona.nombre LIKE %:filtro% OR persona.apellido LIKE %:filtro%",
            nativeQuery = true
    )
    List<Persona> searchNativo(@Param("filtro") String filtro);
    */
}
